(function () {
    angular.module("MyApp").service("MyAppService", MyAppService);

    MyAppService.$inject = ["$http", "$q"];

    function MyAppService($http, $q) {
        var service = this;

        //expose the following services
        // service.getImages = getImages;
        service.getBooks = getBooks;
        service.getBookReview = getBookReview;

        function getBookReview(isbn) {
            var defer = $q.defer();
            $http.get("http://idreambooks.com/api/books/reviews.json", {
                params: {
                    q: isbn,
                    key: "aa2986a875dc549625c41f285e7b9190fef7fa06"
                }
            }).then(function (result) {
                console.log(result);
                var reviews = result.data;
                defer.resolve(reviews);
            }, function (err) {
                console.log(err);
                deferred.reject(err);
            });

            return defer.promise;
        }


        function getBooks() {
            var defer = $q.defer();
            $http.get("https://api.nytimes.com/svc/books/v3/lists/overview.json", {
                params: {
                    api_key: "5977b3bc1a6e4f6aa7e60329ac4302e2"
                }
            }).then(function (result) {
                // console.log(result);
                var books = result.data;
                defer.resolve(books);
            }, function (err) {
                console.log(err);
                deferred.reject(err);
            });

            return defer.promise;
        }


        // function getImages(tag) {

        //     var defer = $q.defer();

        //     $http.get("https://api.giphy.com/v1/gifs/search", {
        //         params: {
        //             api_key: "ac419443623246189d35e51eab60c1d9",
        //             q: tag,
        //             limit: 5,
        //             offset: 0
        //         }
        //     }).then(function (result) {
        //         console.log(result.data.data[0]);
        //         var data = result.data.data;
        //         var images = []
        //         for (var i in data) {
        //             images.push(data[i].images.fixed_height_small.url);
        //             // images.push(data[i].images.downsized_medium.url);      
        //         }
        //         var tag = self.tag;
        //         defer.resolve(images);
        //     }, function (err) {
        //         deferred.reject(err);
        //     });

        //     return defer.promise;
        // }

    }

})();
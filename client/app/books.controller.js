(function () {
    "use strict";
    angular.module("MyApp").controller("BooksCtrl", BooksCtrl);

    BooksCtrl.$inject = ["$state", "MyAppService"];

    function BooksCtrl($state, MyAppService) {
        console.log("BooksCtrl");
        var self = this; // vm
        // var self.books = [];
        self.results = null;
        self.lists = [];
        self.listnames = [];
        self.selectedList = null;
        self.selectedListIndex = 0;
        self.showResults = false;

        self.showReviews = function(book) {
            // console.log(book.title + " " + book.primary_isbn13);
            $state.go("reviews", { title: book.title, isbn13: book.primary_isbn13 });
        }

        self.initForm = function () {
            MyAppService.getBooks()
                .then(function (result) {
                    // console.log(result);
                    // self.reply = result.data;
                    // self.msg = "Numbooks: " + result.num_results;
                    self.results = result.results;
                    self.lists = result.results.lists;
                    // console.log(self.lists.length);
                    for (var i in self.lists) {
                        self.listnames.push(self.lists[i].list_name);
                    }
                    self.selectedList = self.lists[0];
                    // console.log(self.selectedList);
                    self.selectedListIndex = self.listnames[0];
                    
                    self.showResults = true;
                }).catch(function (e) {
                    console.log(e);
                });
        }

        self.initForm();

        self.showSelectedList = function() {
            console.log(self.selectedListIndex);
            var i =  self.listnames.indexOf(self.selectedListIndex);
            self.selectedList = self.lists[i];
        }

        // self.showSelectedList = function() {
        //     // console.log(self.selectedListIndex);
        //     self.selectedList = self.lists[self.selectedListIndex];
        // }
    }

})();
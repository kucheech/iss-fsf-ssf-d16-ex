(function () {
    "use strict";
    angular.module("MyApp").config(MyConfig);
    MyConfig.$inject = ["$stateProvider", "$urlRouterProvider"];
    
    function MyConfig($stateProvider, $urlRouterProvider) {

        $stateProvider
            .state("books", {
                url: "/books",
                templateUrl: "/views/books.html"                
            })
            .state("reviews", {
                url: "/reviews/:isbn13/:title",
                templateUrl: "/views/reviews.html"
            })

        $urlRouterProvider.otherwise("/books");

    }

   

    
    
})();
(function () {
    "use strict";
    angular.module("MyApp").controller("ReviewsCtrl", ReviewsCtrl);

    ReviewsCtrl.$inject = ["$state", "MyAppService"];

    function ReviewsCtrl($state, MyAppService) {
        console.log("ReviewsCtrl");
        console.log($state.params);
        var self = this; // vm        
        self.reviews = [];
        self.showResults = false;
        self.title = "";
        self.isbn13 = "";

        self.initForm = function () {

            self.title = $state.params.title;
            self.isbn13 = $state.params.isbn13;

            console.log(self.isbn13);
            
            MyAppService.getBookReview(self.isbn13)
                .then(function (result) {
                    console.log(result);
                    // self.reply = result.data;
                    // self.msg = "Numbooks: " + result.num_results;                    
                    self.reviews = result.book.critic_reviews;
                    self.showResults = true;
                }).catch(function (e) {
                    console.log(e);
                });
        }

        self.initForm();
 
    }

})();